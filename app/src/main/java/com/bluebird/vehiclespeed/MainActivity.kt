package com.bluebird.vehiclespeed

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity(), IBaseGpsListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (checkLocationPermission(this)) {
            val locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                1 * 30 * 1000,
                0f,
                this
            )
            //updateSpeed(null)
        }

        val chkUseMetricUntis = findViewById<CheckBox>(R.id.chkMetricUnits)
        chkUseMetricUntis.setOnCheckedChangeListener { _: CompoundButton, _: Boolean ->
            //updateSpeed(null)
        }

        btn_calculate.setOnClickListener {
            dummyCalculate()
        }
    }

    private fun checkLocationPermission(activity: Activity?): Boolean {
        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 123
            )
            return false
        }
        return true
    }

    override fun finish() {
        super.finish()
        exitProcess(0)
    }

    private fun updateSpeed(location: CLocation?) {
        var nCurrentSpeed = 0f
        if (location != null) {
            location.setUseMetricunits(useMetricUnits())
            nCurrentSpeed = location.speed
        }
        val fmt = Formatter(StringBuilder())
        fmt.format(Locale.US, "%5.1f", nCurrentSpeed)
        var strCurrentSpeed: String = fmt.toString()
        strCurrentSpeed = strCurrentSpeed.replace(' ', '0')
        var strUnits = "miles/hour"
        if (useMetricUnits()) {
            strUnits = "meters/second"
        }
        val txtCurrentSpeed = findViewById<TextView>(R.id.txtCurrentSpeed)
        txtCurrentSpeed.text = "$strCurrentSpeed $strUnits"
    }

    private fun useMetricUnits(): Boolean {
        val chkUseMetricUnits = findViewById<CheckBox>(R.id.chkMetricUnits)
        return chkUseMetricUnits.isChecked
    }

    override fun onLocationChanged(location: Location?) {
        if (location != null) {
            //val myLocation = CLocation(location, useMetricUnits())
            //updateSpeed(myLocation)
            Log.e("locationChange", location.toString())
            locationChanged(location)
        }
    }

    override fun onProviderDisabled(provider: String?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onGpsStatusChanged(event: Int) {
    }

    /** Calculate distance and speed **/

    private fun dummyCalculate() {
        val lat1 = -6.252370
        val lng1 = 106.826883
        val lat2 = -6.246063
        val lng2 = 106.825965
        val result = calculateDistanceInKilometer(lat1, lng1, lat2, lng2)
        txtCurrentSpeed.text = "$result Km/s"
    }

    var lastLocation: Location? = null
    private fun locationChanged(location: Location) {
        if (lastLocation == null) {
            lastLocation = location
            return
        }
        val deltaTime =
            (location.time - (lastLocation?.time ?: 0)).toFloat() / 3600000  // result hour
        val distance = calculateDistanceInKilometer(
            lastLocation?.latitude ?: 0.0,
            lastLocation?.longitude ?: 0.0,
            location.latitude,
            location.longitude
        )
        val velocity = if (deltaTime > 0) distance / deltaTime else 0F

        txtCurrentSpeed.text =
            "Distance = $distance km \nTime =  $deltaTime jam \nSpeed = $velocity Km/jam"
        Log.e(
            "Location",
            "\nDistance = $distance km \nTime =  $deltaTime jam \nSpeed = $velocity Km/jam"
        )
        lastLocation = location
    }

    private fun calculateDistanceInKilometer(
        lat1: Double,
        lng1: Double,
        lat2: Double,
        lng2: Double
    ): Float {
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lng2 - lng1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(
            Math.toRadians(lat2)
        ) * Math.sin(dLon / 2) * Math.sin(dLon / 2)
        val c = 2 * Math.asin(Math.sqrt(a))
        val distanceInMeters = Math.round(6371000 * c).toFloat()
        return distanceInMeters / 1000
    }

}

